'''
File: xgboost.py
Project: models
File Created: Saturday, 12th June 2021 3:25:30 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:26:40 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''
from ..metrics.classification import ClsClassificationMetrics
from ..metrics.regression import ClsRegressionMetrics
from ..optimizer.optuna import ClsOptunaOptimizer
from .base import ClsBaseModel

from ...imports import (plt,
                        xgb,
                        shap,
                        skmetrics)

__all__ = [
    'ClsXgboostModel'
]

class ClsXgboostModel(ClsBaseModel):
    def __init__(self, xgboost_config, data_module):
        """
        """
        # Instantiating the base model loader class
        super().__init__()
        
        # Setting the data_module and xgboost config
        self.data_module = data_module
        self.xgboost_config = xgboost_config

        # Initialize Model Attributes
        self._initialize_model_attributes()

    def _initialize_model_attributes(self):
        """
        """
        # 1. Check what is the task
        if self.xgboost_config['task'] == 'classification':
            # 2. Choosing the right scoring function for classification default f1
            scoring_function_name = self.xgboost_config.get('scoring_function_name', 'f1')
            self.scoring_function = ClsClassificationMetrics(metric=scoring_function_name)

            # 2a. We are setting if this classification is binary or multiclass
            self.num_class = self.data_module.ytrain.nunique()

        elif self.xgboost_config['task'] == 'regression':
            # 2. Choosing the right scoring function for regression default r2
            scoring_function_name = self.xgboost_config.get('scoring_function_name', 'r2')
            self.scoring_function = ClsRegressionMetrics(metric=scoring_function_name)

        else:
            raise ValueError('task got incorrect Value. Expected value from [regression, classification]')

        # 3. Check if we need to do hyperparameter tuning
        self.enable_tuning = self.xgboost_config['hyperparmeter_tuning'].get('enable_tuning', False)
            
        if self.enable_tuning == True:
            # Initialzing optimizer attributes
            self._initialize_optimizer_attributes()
        
        # 4. Check if we need to shap analysis 
        self.enable_shap = self.xgboost_config['shap_analysis'].get('enable_shap', False)
            
        if self.enable_shap == True:
            # Initialzing shap attributes
            self._initialize_shap_attributes()

        # 5. Setting up a fit_flag
        self._fit_flag = False

        # 6. Initializing the data in XGB DMatrix
        ## Train & Test only X data transformation 
        self.xgb_train = xgb.DMatrix(self.data_module.xtrain)
        self.xgb_test = xgb.DMatrix(self.data_module.xtest)

        ## Train & Test Full X and Y data transformation
        self.xgb_train_data = xgb.DMatrix(self.data_module.xtrain, self.data_module.ytrain)
        self.xgb_test_data = xgb.DMatrix(self.data_module.xtest, self.data_module.ytest)

    def _initialize_shap_attributes(self):
        """
        """
        pass

    def _initialize_optimizer_attributes(self):
        """
        """
        self.optimizer_type = self.xgboost_config['hyperparmeter_tuning'].get('optimizer', 'optuna')

        if self.optimizer_type == 'optuna':
            self.optimizer_parameters = self.xgboost_config['hyperparmeter_tuning']['optimizer_parameters']

        else:
            raise NotImplementedError
    
    def _create_vanilla_xgboost(self, parameters):
        """
        """
        if self.xgboost_config['task'] == 'classification':
            # TODO: Check if objective function can be changed for binary classification
            parameters['objective'] = 'multi:softmax'
            parameters['num_class'] = self.num_class
            
            model = xgb.train(params=parameters,
                              dtrain=self.xgb_train_data,
                              evals=[(self.xgb_train_data, 'train'), (self.xgb_test_data, 'valid')],
                              verbose_eval=False,
                              num_boost_round=500,
                              early_stopping_rounds=10)

        elif self.xgboost_config['task'] == 'regression':
            model = xgb.train(params=parameters,
                              dtrain=self.xgb_train_data,
                              evals=[(self.xgb_train_data, 'train'), (self.xgb_test_data, 'valid')],
                              verbose_eval=False,
                              num_boost_round=500,
                              early_stopping_rounds=10)

        else:
            raise ValueError('task got incorrect Value. Expected value from [regression, classification]')

        return model
    
    def _model_scoring(self, model, return_score=False):
        """
            Internal function to use model scoring
            for hyperparameter tuning
        """
        # This function is used to score the different trial 
        # while hyperparamter tuning
        ytrain_pred = model.predict(self.xgb_train)
        ytest_pred = model.predict(self.xgb_test)
        
        if return_score == True:
            train_score = self.scoring_function(y_true=self.data_module.ytrain, y_pred=ytrain_pred)
            test_score = self.scoring_function(y_true=self.data_module.ytest, y_pred=ytest_pred)

            return (train_score, test_score)

    def _evaluate_model(self):
        """
        """
        # 1. Check the fit_flag 
        if self._fit_flag == True:
            # 2. Get model prediction
            ytrain_pred = self.trained_model.predict(self.xgb_train)
            ytest_pred = self.trained_model.predict(self.xgb_test)

            train_score = self.scoring_function(y_true=self.data_module.ytrain, y_pred=ytrain_pred)
            test_score = self.scoring_function(y_true=self.data_module.ytest, y_pred=ytest_pred)

            # 3. Print Report
            print(str(self.scoring_function) + ' Score On Train: {0} & Test: {1}\n'.format(train_score, test_score))

            if self.xgboost_config['task'] == 'classification':
                # 4. Generate classification report
                cls_rpt_train = skmetrics.classification_report(self.data_module.ytrain, ytrain_pred)
                cls_rpt_test = skmetrics.classification_report(self.data_module.ytest, ytest_pred)

                print('Classification Report On \nTrain: {0} \n Test: {1}'.format(cls_rpt_train, cls_rpt_test))

            elif self.xgboost_config['task'] == 'regression':
                pass
            
            else:
                raise ValueError('task got incorrect Value. Expected value from [regression, classification]')
        else:
            raise Exception('Model Not Fitted. Please call the train() method!')

    def train(self):
        """
        """
        # 1. We will check if hyperparameter tuning in enabled or not
        if self.enable_tuning == True:
            if self.optimizer_type == 'optuna':
                # 2. We need to setup the configuration for optuna
                optuna_config = {
                    'seed' : self.optimizer_parameters.get('seed', 42),
                    'model_config' : {
                        'direction' : self.optimizer_parameters.get('direction', 'maximize'),
                        'scoring_function' : self._model_scoring,
                        'model_to_optimize' : self._create_vanilla_xgboost,
                    },
                    'sampler_config' : {
                        'sampler' : self.optimizer_parameters.get('sampler', 'TPESampler')
                    },
                    'optuna_params_config' : {
                        'fixed_parameters' : self.optimizer_parameters['fixed_parameters'],
                        'varying_parameters' : self.optimizer_parameters['varying_parameters'],
                    },
                }
                
                # 3. We will setup the optuna optimizer & start the optimization process
                self._optuna_obj = ClsOptunaOptimizer(optuna_config=optuna_config)
                self._optuna_obj.start_optimization()

                # 4. Extract the best parameters from optuna hyperparamter tuning
                self.model_best_parameters = self._optuna_obj.study.best_params
            
            else:
                raise NotImplementedError
                    
        else:
            # If not enabled then we will just fit the model
            # We will use the default hyperparameters
            self.model_best_parameters = self.xgboost_config.get('default_hyperparameters', dict())

        # Let's train the model on model_best_parameters depending the configuration
        self.trained_model = self._create_vanilla_xgboost(parameters=self.model_best_parameters)

        # Setting the fit flag to True
        if self._fit_flag == False:
            self._fit_flag = True

    def predict(self, data):
        """
            Function helps to predict on new data
        """
        if self._fit_flag == True:
            return self.trained_model.predict(xgb.DMatrix(data))
        else:
            raise Exception('Model Not Fitted. Please call the train() method!')

    def run_shap_analysis(self):
        """
        """
        if self._fit_flag == True:
            # 1. Instatiating the shap Tree Explainer
            self.shap_explainer = shap.TreeExplainer(self.trained_model)

            # 2. Here the explainer will generate shap values for train data
            self.shap_values = self.shap_explainer.shap_values(xgb.DMatrix(self.data_module.xtrain)) 
            
            # 3. Evaluate Model Performance
            self._evaluate_model()

            # TODO: Add support for classification/Regression -- plots
            # 4. Plot these shap values as tornado plot and summary bar plot 
            plt.figure(figsize=(15,7))
            shap.summary_plot(shap_values=self.shap_values, 
                              features=self.data_module.xtrain, 
                              max_display=30)

            plt.figure(figsize=(15,7))
            shap.summary_plot(shap_values=self.shap_values, 
                              features=self.data_module.xtrain, 
                              max_display=30, 
                              plot_type='bar')

        else:
            raise Exception('Model Not Fitted. Please call the train() method!')

    def save_model_artifacts(self):
        """
        """
        pass