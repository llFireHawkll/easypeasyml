'''
File: base.py
Project: models
File Created: Saturday, 12th June 2021 3:25:14 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:25:44 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''
from ...imports import (ABC,
                        abstractmethod)

__all__ = [
    'ClsBaseModel'
]


class ClsBaseModel(ABC):
    def __init__(self):
        """
        """
        pass
    
    @abstractmethod
    def train(self):
        """
        """
        pass

    @abstractmethod
    def predict(self, data):
        """
        """
        pass

    @abstractmethod
    def save_model_artifacts(self):
        """
        """
        pass