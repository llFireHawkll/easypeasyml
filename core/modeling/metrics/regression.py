'''
File: regression.py
Project: metrics
File Created: Saturday, 12th June 2021 3:20:49 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:23:43 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''
from ...imports import (np,
                        skmetrics)

__all__ = [
    'ClsRegressionMetrics'
]

class ClsRegressionMetrics:
    def __init__(self, metric):
        """
            Constructor class for regression metrics
        """
        self.metrics = {
                "r2" : self._r2,
                "mae" : self._mae,
                "mse" : self._mse,
                "msle" : self._msle,
                "rmse" : self._rmse,
                "rmsle" : self._rmsle,
                
        }
        self.metric = metric
        
        if self.metric not in self.metrics:
            raise Exception("Invalid metric passed")

    def __call__(self, y_true, y_pred):
        return self.metrics[self.metric](y_true, y_pred)

    @staticmethod
    def _mae(y_true, y_pred):
        return skmetrics.mean_absolute_error(y_true=y_true, y_pred=y_pred)

    @staticmethod
    def _msle(y_true, y_pred):
        return skmetrics.mean_squared_log_error(y_true=y_true, y_pred=y_pred)

    @staticmethod
    def _mse(y_true, y_pred):
        return skmetrics.mean_squared_error(y_true=y_true, y_pred=y_pred)

    def _rmsle(self, y_true, y_pred):
        return np.sqrt(self._msle(y_true=y_true, y_pred=y_pred))

    def _rmse(self, y_true, y_pred):
        return np.sqrt(self._mse(y_true=y_true, y_pred=y_pred))

    @staticmethod
    def _r2(y_true, y_pred):
        return skmetrics.r2_score(y_true=y_true, y_pred=y_pred)

    def __repr__(self):
        return str(self.metric).upper()

    def __str__(self):
        return self.__repr__()