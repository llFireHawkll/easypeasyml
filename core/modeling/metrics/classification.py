'''
File: classification.py
Project: metrics
File Created: Saturday, 12th June 2021 3:20:41 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:24:46 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''
from ...imports import (np,
                        skmetrics)

__all__ = [
    'ClsClassificationMetrics'
]

class ClsClassificationMetrics:
    def __init__(self, metric):
        """
        init class for classification metrics
        """
        self.metrics = {
                "f1" : self._f1,
                "auc" : self._auc,
                "kappa" : self._kappa,
                "recall": self._recall,
                "logloss" : self._log_loss,
                "accuracy": self._accuracy,
                "precision" : self._precision,
                "quadratic_kappa": self._quadratic_weighted_kappa,
                
        }

        self.metric = metric
        
        if self.metric not in self.metrics:
            raise Exception("Invalid metric passed")

    def __call__(self, y_true, y_pred, y_proba=None):
        if self.metric == "auc":
            if y_proba is not None:
                return self.metrics[self.metric](y_true, y_proba[:, 1])
            else:
                return np.nan

        elif self.metric == "logloss":
            if y_proba is not None:
                return self.metrics[self.metric](y_true, y_proba[:, 1])
            else:
                return np.nan

        elif self.metric == "multiclass_logloss":
            if y_proba is not None:
                return self.metrics[self.metric](y_true, y_proba)
            else:
                return np.nan
                
        else:
            return self.metrics[self.metric](y_true, y_pred)

    @staticmethod
    def _accuracy(y_true, y_pred):
        return skmetrics.accuracy_score(y_true=y_true, y_pred=y_pred)

    def _auc(self, y_true, y_pred):
        return skmetrics.roc_auc_score(y_true=y_true, y_score=y_pred)

    @staticmethod
    def _f1(y_true, y_pred):
        return skmetrics.f1_score(y_true=y_true, y_pred=y_pred)

    @staticmethod
    def _log_loss(y_true, y_pred):
        return skmetrics.log_loss(y_true=y_true, y_pred=y_pred)

    @staticmethod
    def _kappa(y1, y2, weights):
        return skmetrics.cohen_kappa_score(y1=y1, y2=y2, weights=weights)

    @staticmethod
    def _precision(y_true, y_pred):
        return skmetrics.precision_score(y_true=y_true, y_pred=y_pred)

    @staticmethod
    def _quadratic_weighted_kappa(y1, y2):
        return skmetrics.cohen_kappa_score(y1, y2, weights="quadratic")

    @staticmethod
    def _recall(y_true, y_pred):
        return skmetrics.recall_score(y_true=y_true, y_pred=y_pred)

    def __repr__(self):
        return str(self.metric).upper()

    def __str__(self):
        return self.__repr__()