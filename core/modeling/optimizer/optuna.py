'''
File: optuna.py
Project: optimizer
File Created: Saturday, 12th June 2021 3:27:15 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:27:34 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''

from ...exceptions import (VariableInitializationException)
from ...imports import optuna

__all__ = [
    'ClsOptunaOptimizer'
]

class ClsOptunaOptimizer:
    def __init__(self, optuna_config):
        """
        """
        # Fetching various details from the config
        # Seed
        self.seed = optuna_config.get('seed', 42)

        # Model Configuration required for optuna
        self.model_config = optuna_config.get('model_config', dict())

        # Sampler Configuration required for optuna
        self.sampler_config = optuna_config.get('sampler_config', {})

        # Optuna Parameter Configuration
        self.optuna_params_config = optuna_config.get('optuna_params_config', dict())
        
        # Run variable initiations checks
        self._check_variable_initiations()

        # Initiating the Optimizer
        self.setup_optimizer()

    def _check_hasattr(self, attribute):
        """
        """
        if hasattr(self, attribute):
            pass
        else:
            raise VariableInitializationException

    def _check_variable_initiations(self):
        """
        """
        # 1. Check if all the required variables are initialized
        self._check_hasattr(attribute='seed')
        self._check_hasattr(attribute='model_config')
        self._check_hasattr(attribute='sampler_config')
        self._check_hasattr(attribute='optuna_params_config')

    @property
    def sampler(self):
        """
        """
        return self.__sampler
    
    @sampler.setter
    def sampler(self, sampler_config):
        """
        """
        if sampler_config['sampler'] == 'TPESampler':
            self.__sampler = optuna.samplers.TPESampler(seed=self.seed) 
        else:
            raise NotImplementedError
    
    def _objective_function(self, trial):
        """
            Defining the objective function
        """
        # defining an empty dict
        trial_parameters = dict()

        # 1. Setting the first parameter nthread to be -1
        trial_parameters['nthread'] = -1

        # 2. Now we will be reading the varying parameters
        # from the model configuration
        dict_varying_params = self.optuna_params_config.get('varying_parameters', dict())

        for parameter in dict_varying_params.keys():
            if type(dict_varying_params[parameter]) == list:
                trial_parameters[parameter] = trial.suggest_categorical(name=parameter, 
                                                                        choices=dict_varying_params[parameter])

            elif dict_varying_params[parameter].get('step'):
                min_value = dict_varying_params[parameter]['min']
                max_value = dict_varying_params[parameter]['max']
                step_value = dict_varying_params[parameter]['step']

                trial_parameters[parameter] = trial.suggest_discrete_uniform(name=parameter,
                                                                             low=min_value,
                                                                             high=max_value,
                                                                             q=step_value)
            else:
                min_value = dict_varying_params[parameter]['min']
                max_value = dict_varying_params[parameter]['max']

                if type(min_value) == int:
                    trial_parameters[parameter] = trial.suggest_int(name=parameter,
                                                                    low=min_value,
                                                                    high=max_value)                                    
                else:
                    trial_parameters[parameter] = trial.suggest_loguniform(name=parameter,
                                                                           low=min_value,
                                                                           high=max_value)
        
        # Loading the model to optimize
        model = self.model_config['model_to_optimize'](parameters=trial_parameters)

        # Running the model and scoring it for that set of hyperparameters
        ## we are interested in validation score
        _, valid_score = self.model_config['scoring_function'](model=model, return_score=True)
        
        return valid_score

    def setup_optimizer(self):
        """
            Function to setup the optuna 
            Sampler and Study
        """
        # Setting the Sampler using property
        self.sampler = self.sampler_config

        # We will be also in initiate the study and 
        # start the optimizing process
        self.study = optuna.create_study(direction=self.model_config['direction'], 
                                         sampler=self.sampler)

    def start_optimization(self):
        """
            In this function we will
            run the optimization process
        """        
        # Here we start the optimization 
        self.study.optimize(func=self._objective_function, 
                            n_trials=self.optuna_params_config['fixed_parameters']['n_trials'])