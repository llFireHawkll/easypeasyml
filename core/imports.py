'''
File: imports.py
Project: core
File Created: Saturday, 12th June 2021 3:21:06 pm
Author: Sparsh Dutta (sparsh.dtt@gmail.com)
-----
Last Modified: Saturday, 12th June 2021 3:22:24 pm
Modified By: Sparsh Dutta (sparsh.dtt@gmail.com>)
-----
Copyright 2021 Sparsh Dutta
'''
from sklearn.preprocessing import LabelEncoder, OrdinalEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics as skmetrics
from IPython.core.display import display
from abc import ABC, abstractmethod
from tqdm.notebook import tqdm
from loguru import logger

import io
import os
import copy
import json
import shap
import boto3
import mlflow
import optuna
import pytest
import typing
import warnings
import numpy as np
import pandas as pd
import xgboost as xgb
import seaborn as sns
import plotly.graph_objs as go
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import plotly.figure_factory as ff

warnings.filterwarnings("ignore", category=DeprecationWarning)